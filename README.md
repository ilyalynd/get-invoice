# Get Invoice

## Project setup

```text
npm install
```

### Compiles and hot-reloads for development

```text
npm run dev
```

### Compiles and minifies for production

```text
npm run build
```

### Lints and fixes files

```text
npm run lint
```
